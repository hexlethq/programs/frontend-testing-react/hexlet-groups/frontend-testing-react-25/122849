const faker = require('faker');

// BEGIN
test('should create unique transacrions', () => {
  const transaction = faker.helpers.createTransaction();
  const anotherTransaction = faker.helpers.createTransaction();

  expect(transaction).not.toEqual(anotherTransaction);
});

const properties = ['amount', 'date', 'business', 'name', 'type', 'account'];

test.each(properties)('should have %s property', (p) => {
  const transaction = faker.helpers.createTransaction();
  expect(transaction).toHaveProperty(p);
});

// END
