# Комплексные и недетерминированные данные

Протестируйте метод `helpers.createTransaction()` пакета [Faker.js](https://github.com/Marak/faker.js)

## Тест кейсы

* Каждая создаваемая транзакция уникальна
* Проверьте наличие у транзакции свойств: `amount`, `date`, `business`, `name`, `type`, `account`
