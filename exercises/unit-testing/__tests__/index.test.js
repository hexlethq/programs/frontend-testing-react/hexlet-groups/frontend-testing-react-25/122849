test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  expect.hasAssertions();

  // BEGIN
  expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
  expect(src).toEqual({ k: 'v', b: 'b' });
  // END
});
