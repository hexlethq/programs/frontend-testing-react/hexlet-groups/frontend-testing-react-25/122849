const axios = require('axios');

// BEGIN
const get = async (url) => {
  const response = await axios.get(url);
  return response.data;
};

const post = (url, body) => axios.post(url, body);
// END

module.exports = { get, post };
