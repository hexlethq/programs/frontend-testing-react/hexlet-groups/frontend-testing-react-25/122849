const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN

beforeAll(() => nock.disableNetConnect());

afterAll(() => {
  nock.cleanAll();
  nock.enableNetConnect();
});

const user = {
  firstname: 'Fedor',
  lastname: 'Sumkin',
  age: 33,
};

test('get', async () => {
  nock('https://example.com')
    .get('/users')
    .reply(200, [user]);

  const users = await get('https://example.com/users');
  expect(users).toEqual([user]);
});

test('post', async () => {
  const scope = nock('https://example.com')
    .post('/users')
    .reply(201, [user]);

  const { data } = await post('https://example.com/users', user);
  expect(data).toEqual([user]);
  expect(scope.isDone()).toBe(true);
});
// END
