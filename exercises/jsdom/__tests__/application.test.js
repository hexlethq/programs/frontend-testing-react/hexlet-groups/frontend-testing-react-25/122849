// @ts-check

require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');

const run = require('../src/application');

beforeEach(() => {
  const initHtml = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  document.body.innerHTML = initHtml;
  run();
});

// BEGIN
it('should correctly init app', () => {
  const listsContainer = document.querySelector('[data-container="lists"]');
  const tasksContainer = document.querySelector('[data-container="tasks"]');
  expect(listsContainer.querySelectorAll('li')).toHaveLength(1);
  expect(tasksContainer).toBeEmptyDOMElement();
});

it('should add task to default list', () => {
  const taskInput = document.querySelector('[data-testid="add-task-input"]');
  taskInput.value = 'new task';
  const addTaskBtn = document.querySelector('[data-testid="add-task-button"]');
  addTaskBtn.click();
  expect(taskInput).toHaveValue('');
  const listsContainer = document.querySelector('[data-container="lists"]');
  const tasksContainer = document.querySelector('[data-container="tasks"]');
  expect(tasksContainer).toHaveTextContent('new task');
  expect(listsContainer).toHaveTextContent('General');
});

it('should add multiple tasks', () => {
  const tasks = ['first task', 'second task'];
  const taskInput = document.querySelector('[data-testid="add-task-input"]');
  const addTaskBtn = document.querySelector('[data-testid="add-task-button"]');
  tasks.forEach((task) => {
    taskInput.value = task;
    addTaskBtn.click();
  })
  const tasksContainer = document.querySelector('[data-container="tasks"]');
  const addedTasks = [...tasksContainer.querySelectorAll('li')].map(el => el.textContent);
  expect(addedTasks).toEqual(tasks);
});

it('should add new list', () => {
  const listInput = document.querySelector('[data-testid="add-list-input"]');
  const addListBtn = document.querySelector('[data-testid="add-list-button"]');
  const listsContainer = document.querySelector('[data-container="lists"]');

  listInput.value = 'new list';
  addListBtn.click();

  expect(listsContainer).toHaveTextContent('new list');
});

it('should add task to the new list', () => {
  const taskInput = document.querySelector('[data-testid="add-task-input"]');
  const addTaskBtn = document.querySelector('[data-testid="add-task-button"]');
  const listInput = document.querySelector('[data-testid="add-list-input"]');
  const addListBtn = document.querySelector('[data-testid="add-list-button"]');
  const listsContainer = document.querySelector('[data-container="lists"]');
  const tasksContainer = document.querySelector('[data-container="tasks"]');

  taskInput.value = 'new task';
  addTaskBtn.click();
  listInput.value = 'new list';
  addListBtn.click();

  listsContainer.querySelector('li a').click();

  taskInput.value = 'new list task';
  addTaskBtn.click();

  expect(tasksContainer).not.toHaveTextContent('new task');
  expect(tasksContainer).toHaveTextContent('new list task');

  listsContainer.querySelector('li a').click();

  expect(tasksContainer).toHaveTextContent('new task');
  expect(tasksContainer).not.toHaveTextContent('new list task');
})
// END
