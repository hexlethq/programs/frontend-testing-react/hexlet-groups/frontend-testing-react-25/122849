const port = 5000;
const appUrl = `http://localhost:${port}`;

describe('simple blog works', () => {
  beforeAll(async () => {
    await page.goto(appUrl);
  });

  it('should open page', async () => {
    await expect(page).toMatch('Welcome to a Simple blog!');
  });

  it('should open articles page', async () => {
    await expect(page).toClick('[data-testid="nav-articles-index-link"]');
    await page.waitForNavigation();
    await expect(page).toMatchElement('table');
  });

  it('shoul open new article form', async () => {
    await expect(page).toClick('[data-testid="article-create-link"]');
    await page.waitForNavigation();
    await expect(page).toMatchElement('form');
  });

  it('should fill form and create new article', async () => {
    await expect(page).toMatchElement('form');
    await expect(page).toFill('#name', 'new article');
    await expect(page).toFill('#content', 'new article content');
    await expect(page).toSelect('#category', 'optio quo quis');
    await expect(page).toClick('[data-testid="article-create-button"]');
    await page.waitForNavigation();
    await expect(page).toMatchElement('td', { text: 'new article' });
  });

  it('should edit article', async () => {
    await expect(page).toClick('[data-testid="article-edit-link-4"]');
    await page.waitForNavigation();
    await expect(page).toFill('#name', 'updated article');
    await expect(page).toClick('[data-testid="article-update-button"]');
    await page.waitForNavigation();
    await expect(page).toMatchElement('td', { text: 'updated article' });
  });
});

// END
