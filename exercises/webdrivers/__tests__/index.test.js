const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-gpu'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });
  // BEGIN

  test('should open main page', async () => {
    await page.goto(appUrl);
    await page.waitForSelector('#title');
    const text = await page.$eval('#title', (e) => e.textContent);
    expect(text).toBe('Welcome to a Simple blog!');
  });

  test('should open articles page', async () => {
    const articlesLink = await page.$('.nav-link');
    await articlesLink.click();
    await page.waitForSelector('#articles');
  });

  test('should open new article form', async () => {
    const createNewArticleBtn = await page.$('[href="/articles/new"]');
    await createNewArticleBtn.click();
    await page.waitForSelector('form');
  });

  test('should fill form and create new article', async () => {
    await page.type('#name', 'new article');
    const submitBtn = await page.$('[type="submit"]');
    await submitBtn.click();
    await page.waitForSelector('#articles');
    const newArticleName = await page.$eval('tbody tr:last-child td:nth-child(2)', (e) => e.textContent);
    expect(newArticleName).toBe('new article');
  });

  test('should edit article', async () => {
    const newArticleEditBtn = await page.$('tbody tr:last-child td:nth-child(4) a');
    await newArticleEditBtn.click();
    await page.waitForSelector('form');
    const input = await page.$('#name');
    await input.click({ clickCount: 3 });
    await input.type('updated article');
    const submitBtn = await page.$('[type="submit"]');
    await submitBtn.click();
    await page.waitForSelector('#articles');
    const newArticleName = await page.$eval('tbody tr:last-child td:nth-child(0)', (e) => e.textContent);
    expect(newArticleName).toBe('updated article');
  });

  // END

  afterAll(async () => {
    await browser.close();
    await app.close();
  });
});
