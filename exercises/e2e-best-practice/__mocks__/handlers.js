/* eslint-disable no-undef */
/* eslint-disable import/no-extraneous-dependencies */
import { rest } from 'msw';
import getNextId from '../lib/getNextId';

export const handlers = [
  rest.get('/tasks', (req, res, ctx) => {
    const savedTasks = JSON.parse(sessionStorage.getItem('tasks')) || [];
    return res(
      ctx.status(200),
      ctx.json(savedTasks),
    );
  }),
  rest.post('/tasks', (req, res, ctx) => {
    const newTask = {
      state: 'active',
      id: getNextId(),
      text: req.body.task.text,
    };

    const savedTasks = JSON.parse(sessionStorage.getItem('tasks')) || [];

    sessionStorage.setItem('tasks', JSON.stringify([newTask, ...savedTasks]));

    return res(
      ctx.status(200),
      ctx.json(newTask),
    );
  }),
  rest.delete('/api/v1/tasks/:id', (req, res, ctx) => res(
    ctx.status(204),
  )),
];