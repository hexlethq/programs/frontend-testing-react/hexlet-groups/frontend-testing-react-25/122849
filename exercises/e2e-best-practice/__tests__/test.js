// @ts-check
// BEGIN
import 'expect-puppeteer'

describe('simple todo list work', () => {
  beforeAll(async () => {
    await page.goto('http://localhost:8080/');
  });
  
  it('should open page', async () => {
    await expect(page).toMatchElement('[data-testid="add-task-button"]');
    await expect(page).toMatchElement('[data-testid="task-name-input"]');
  });

  it('should add task', async () => {
    await expect(page).toFill('[data-testid="task-name-input"]', 'new task')
    await expect(page).toClick('[data-testid="add-task-button"]');
    await expect(page).toMatchElement('ul li', { text: 'new task'});
  });

  it('should delete task', async () => {
    await expect(page).toClick('[data-testid="remove-task-1"]');
    await expect(page).not.toMatchElement('ul li', { text: 'new task'});
  });
});
// END
