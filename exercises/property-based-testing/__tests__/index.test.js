const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
test('should be sorted', () => {
  fc.assert(
    fc.property(fc.array(fc.integer()), (data) => {
      const result = sort(data);
      expect(result).toBeSorted();
    }),
  );
});

it('should have the same length as source', () => {
  fc.assert(
    fc.property(fc.array(fc.integer()), (data) => {
      expect(sort(data)).toHaveLength(data.length);
    }),
  );
});
// END
