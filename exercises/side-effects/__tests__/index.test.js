const fs = require('fs');
// eslint-disable-next-line no-unused-vars
const { version } = require('os');
const path = require('path');
const { upVersion } = require('../src/index.js');

// BEGIN
const getFixturePath = (filename) => path.join(__dirname, '..', '__fixtures__', filename);
const readFile = (filename) => fs.readFileSync(getFixturePath(filename), 'utf-8');
const noop = () => {};

let packageData;

beforeEach(() => {
  packageData = readFile('package.json');
  fs.writeFileSync(getFixturePath('packageCopy.json'), packageData);
});

afterEach(() => {
  fs.unlink(getFixturePath('packageCopy.json'), noop);
});

const versionTypes = [
  ['patch', '{"version":"1.3.3"}'],
  ['minor', '{"version":"1.4.0"}'],
  ['major', '{"version":"2.0.0"}'],
];

test.each(versionTypes)('should update %s version', (type, expected) => {
  upVersion(getFixturePath('packageCopy.json'), type);
  const updatedPackage = readFile('packageCopy.json');

  expect(updatedPackage).toEqual(expected);
});
// END
