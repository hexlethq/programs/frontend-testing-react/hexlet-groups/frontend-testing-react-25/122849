const fs = require('fs');

// BEGIN
const map = {
  patch: (data) => {
    const [majorVersion, minorVersion, patchVersion] = data.split('.');
    return [majorVersion, minorVersion, Number(patchVersion) + 1].join('.');
  },
  minor: (data) => {
    const [majorVersion, minorVersion] = data.split('.');
    return [majorVersion, Number(minorVersion) + 1, 0].join('.');
  },
  major: (data) => {
    const [majorVersion] = data.split('.');
    return [Number(majorVersion) + 1, 0, 0].join('.');
  },
};

const upVersion = (filepath, partToUpdate = 'patch') => {
  const rawPackageData = fs.readFileSync(filepath, 'utf-8');
  const packageData = JSON.parse(rawPackageData);
  const updatedVersion = map[partToUpdate](packageData.version);
  const updatedPackage = { ...packageData, version: updatedVersion };

  fs.writeFileSync(filepath, JSON.stringify(updatedPackage));
};
// END

module.exports = { upVersion };
